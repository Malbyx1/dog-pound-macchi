/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
 worst looking Dog in the world
@ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{
    private static int breedWeight = 50;

    /**
     * @param name Dog's name
     */
    public Yorkshire(String name)
    {
        super(name);
    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "woof";
    }

    /**
     * Calculates average breedWeight
     * @param inW input weight
     * @return average
     */
    public static int avgBreedWeight(int inW){
        return (breedWeight + inW) / 2;
    }
}