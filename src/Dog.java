/****************************************************************
 Dog.java

 A class that holds a dog's name and can make it speak.

****************************************************************/
public class Dog
{
    protected String name;

    /**
     * Constructor - stores name
     * @param name Dog's name
     */

    public Dog(String name)
    {
        this.name = name;
    }

    /**
     * Getter
     * @return Dog's name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Barking method
     * @return a string of the Dog's comment on life
     */
    public String speak()
    {
        return "Woof";
    }
}