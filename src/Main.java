public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());

        Dog dog1 = new Dog("Spoke");
        Dog dog2 = new Dog("Spake");
        Dog dog3 = new Dog("Piffy");

        Cage cage = new Cage();
        cage.add(dog1);
        cage.add(dog2);
        cage.add(dog3);
        System.out.println(cage.toString());

        LabradorCage lCage = new LabradorCage();
        lCage.transfer(cage);
    }
}
