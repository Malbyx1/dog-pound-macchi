import java.lang.reflect.Array;
import java.util.ArrayList;

public class Cage {
    ArrayList<Dog> cage = new ArrayList<Dog>();

    public Cage(){}

    /**
     * Adds Dog
     * @param dog Dog to add
     */
    public void add(Dog dog){
        cage.add(dog);
    }

    /**
     * Removes said Dog from list
     * @param name Dog's name
     */
    public void remove(String name){
        cage.remove(new Dog(name));
    }

    public String toString(){
        Dog[] dogArr = cage.toArray(new Dog[0]);
        String names = "";
        for(Dog a: dogArr){
            names += " " + a.getName();
        }
        return  "              _\n" +
                "            ,/A\\,\n" +
                "          .//`_`\\\\,\n" +
                "        ,//`____-`\\\\,\n" +
                "      ,//` [MOTHER]`\\\\,\n" +
                "    ,//`= [FUCKERS] _`\\\\,\n" +
                "   //|_________________|\\\\\n" +
                "   ` |  __ .--|--.  _  \\ `\n" +
                "     | - _/   |   \\-   |\n" +
                "     |__  | .-|\"-. | _=|\n" +
                "     |  _=|/) | (\\|    |\n" +
                "     |-__ (/ a|a \\) -__|\n" +
                "     |___ /`\\|Y_/ `\\___|\n" +
                "Imprisoned dogs:" + names;
    }

    /**
     * Makes all Dogs bark
     * @return all the barks
     */
    public String chaos(){
        Dog[] dogArr = cage.toArray(new Dog[0]);
        String barks = "";
        for(Dog a: dogArr){
            barks += a.speak();
        }
        return barks;
    }
}
